#!/usr/bin/python
# -*- coding: UTF-8 -*-
import webbrowser

# 爬虫框架: pip2 install Scrapy
# xml解析: pip2 install lxml
# 文档: https://scrapy-chs.readthedocs.io/zh_CN/0.24/intro/tutorial.html

print"hello scrapy"
print("你好scrapy,这里是爬虫指南")
webbrowser.open("https://scrapy-chs.readthedocs.io/zh_CN/0.24/intro/tutorial.html")

# 创建项目指令: scrapy startproject myproject
# 未安装win32: https://sourceforge.net/projects/pywin32/files/pywin32/Build%20220/
# 并下载: pywin32-220.win-amd64-py2.7.exe
# 启动spider: scrapy crawl dmoz
# 启动spider并保存至csv: scrapy crawl dmoz -o data.csv
