# -*- coding:  UTF-8 -*-
# 创建项目指令: scrapy startproject myproject
# 启动spider: scrapy crawl dmoz
# 启动spider并保存为表格csv: scrapy crawl dmoz -o data.csv
# 启动spider并保存为json: scrapy crawl dmoz -o data.json
import scrapy

from myproject.items import DmozItem


class DmozSpider(scrapy.Spider):
    name = "dmoz"  # 唯一标示,指令要对应
    allowed_domains = ["image.baidu.com"]
    start_urls = [
        # "https://blog.csdn.net/u012468376/article/details/73350998#commentBox",
        "http://image.baidu.com/search/index?tn=baiduimage&ps=1&ct=201326592&lm=-1&cl=2&nc=1&ie=utf-8&word=%E7%8C%AB%E5%92%AA"
    ]

    def parse(self, response):
        for sel in response.xpath('//ul/li'):
            item = DmozItem()
            item['title'] = sel.xpath('a/text()').extract()  # xpath 表达式
            item['link'] = sel.xpath('a/@href').extract()
            item['desc'] = sel.xpath('text()').extract()
            item['img_url'] = sel.xpath('//img/@src').extract()
            yield item
